﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    [SerializeField]
    Cell[] neighbors;

    public RectTransform uiRect;

    public GridChunk Chunk;

    public Vector2Int Position { get { return position; } set { position = value ; } }

    Vector2Int position;

    public void SetNeighbor(Direction direction, Cell cell)
    {
        neighbors[(int)direction] = cell;
        cell.neighbors[(int)direction.Opposite()] = this;
    }
}
