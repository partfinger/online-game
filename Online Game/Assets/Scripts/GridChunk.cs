﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridChunk : MonoBehaviour
{

    Cell[,] cells;
    
    Canvas gridCanvas;

    public Text cellTextPrefab;

    void Awake()
    {
        gridCanvas = GetComponentInChildren<Canvas>();
        cells = new Cell[Metrics.ChunkCellCount, Metrics.ChunkCellCount];
    }

    public void AddCell(int x, int z, Cell cell)
    {
        cells[x, z] = cell;
        cell.transform.SetParent(transform, false);
        cell.Chunk = this;

        Text label = Instantiate(cellTextPrefab);
        label.rectTransform.SetParent(gridCanvas.transform, false);
        label.rectTransform.anchoredPosition =
            new Vector2(cell.Position.x * Metrics.Scale, cell.Position.y * Metrics.Scale);
        label.text = string.Format("{0};{1}", cell.Position.x, cell.Position.y);
    }

}

