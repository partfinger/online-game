﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMap : MonoBehaviour
{
    public int cellCountX, cellCountZ;
    public int chunkCountX, chunkCountZ;

    public Cell cellPrefab;
    public GridChunk chunkPrefab;

    
    Cell[,] cells;
    GridChunk[,] chunks;

    void Awake()
    {
        Clear();
        CreateMap(cellCountX, cellCountZ);
    }

    public void CreateMap(int _x, int _z)
    {
        if ((_x % Metrics.ChunkCellCount == 0) && (_z % Metrics.ChunkCellCount == 0))
        {
            cellCountX = _x;
            cellCountZ = _z;

            chunkCountX = _x / Metrics.ChunkCellCount;
            chunkCountZ = _z / Metrics.ChunkCellCount;

            CreateChunks();
            CreateCells();
        }
        else
        {
            Debug.LogError(string.Format("Кількість клітинок не кратна ємності, {0}, чанка!", Metrics.ChunkCellCount ));
        }
        
    }

    void CreateChunks()
    {
        chunks = new GridChunk[chunkCountX, chunkCountZ];
        for (int z = 0; z < chunkCountZ; z++)
        {
            for (int x = 0; x < chunkCountX; x++)
            {
                GridChunk chunk = chunks[x, z] = Instantiate(chunkPrefab);
                chunk.name = "Chunk[" + Metrics.ChunkCellCount * x + "-" + Metrics.ChunkCellCount * (x + 1) + ";" + Metrics.ChunkCellCount * z + "-" + Metrics.ChunkCellCount * (z + 1) + "]";
                chunk.transform.SetParent(transform);

            }
        }
    }

    void CreateCells()
    {
        cells = new Cell[cellCountX, cellCountZ];
        for (int z = 0; z < cellCountZ; z++)
        {
            for (int x = 0; x < cellCountX; x++)
            {
                CreateCell(x, z);
            }
        }
    }

    void CreateCell(int x, int z)
    {
        Vector3 position;
        position.x = x * Metrics.Scale;
        position.y = 0f;
        position.z = z * Metrics.Scale;
        Cell cell = cells[x,z] = Instantiate(cellPrefab);
        cell.gameObject.transform.localScale = Vector3.one * Metrics.Scale;

        if (x > 0 && z > 0)
        {
            cell.SetNeighbor(Direction.W, cells[x - 1, z]);
            cell.SetNeighbor(Direction.S, cells[x, z - 1]);
        }
        else if (x > 0 && z == 0)
        {
            cell.SetNeighbor(Direction.W, cells[x - 1, z]);
        }
        else if (x == 0 && z > 0)
        {
            cell.SetNeighbor(Direction.S, cells[x, z - 1]);
        }

        cell.name = string.Format("Cell[{0};{1}]",x,z);
        cell.transform.localPosition = position;
        cell.Position = new Vector2Int(x,z);
        
        AddCellToChunk(cell);
    }

    public void Clear()
    {
        if (transform.childCount > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    void AddCellToChunk(Cell cell)
    {
        int chunkX = cell.Position.x / Metrics.ChunkCellCount;
        int chunkZ = cell.Position.y / Metrics.ChunkCellCount;

        GridChunk chunk = chunks[chunkX, chunkZ];
        int localX = cell.Position.x - chunkX * Metrics.ChunkCellCount;
        int localZ = cell.Position.y - chunkZ * Metrics.ChunkCellCount;
        chunk.AddCell(localX, localZ, cell);
    }
}
