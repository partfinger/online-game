﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    N, E, S, W
}

public static class DirectionExtensions
{
    public static Direction Opposite(this Direction direction)
    {
        return (int)direction < 2 ? (direction + 2) : (direction - 2);
    }
}

    public class Metrics : MonoBehaviour
{
    public const float Scale = 10f;
    public const int ChunkCellCount = 5;

}
